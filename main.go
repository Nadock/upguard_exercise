package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

// Vulnerability contains the information of a single vulnerability
type Vulnerability struct {
	Id           int    `json:"id"`
	Severity     int    `json:"severity"`
	Title        string `json:"title"`
	DateReported string `json:"date_reported"`
}

// Vulnerabilities contains all the individual Vulnerability structs and has several associated methods
type Vulnerabilities struct {
	Content []Vulnerability
}

// VulnerabilityFilters represent the selection criteria that filters which vulnerabilities to display.
//
// SeverityAtLeast filters out vulnerabilities with severity less than SeverityAtLeast
// Since filters out vulnerabilities with DateReported prior to the date indicated by the Since string
// Limit restricts the number of responses to at most Limit
type VulnerabilityFilter struct {
	SeverityAtLeast int
	Since           string
	Limit           int
}

func main() {
	var path = flag.String("file", "vulns.json", "path to JSON file containing vulnerabilities")
	var port = flag.String("port", "8080", "the port number to bind the server to")
	flag.Parse()

	vulnerabilities := Vulnerabilities{}
	vulnerabilities.loadVulnerabilities(*path)
	http.HandleFunc("/", vulnerabilities.handler)
	log.Fatal(http.ListenAndServe(":" + *port, nil))
}

// loadVulnerabilities loads the vulnerabilities defined in a JSON file found at path and sets the content array to
// contain the Venerability structs representing each vulnerability in the JSON file.
func (v *Vulnerabilities) loadVulnerabilities(path string) {
	input, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	dec := json.NewDecoder(input)
	err = dec.Decode(&v.Content)
	if err != nil {
		log.Fatal(err)
	}
}

// handler responds to HTTP requests and replies with the request JSON
func (v *Vulnerabilities) handler(w http.ResponseWriter, r *http.Request) {
	vulnerabilitiesJson, err := json.Marshal(v.filterVulnerabilities(r.URL))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Internal Server error"))
	} else {
		fmt.Fprintf(w, "%s", vulnerabilitiesJson)
		log.Print("successfully served request")
	}
}

// filterVulnerabilities will return an array of Vulnerabilities that match the parameters in the URL of the http request
func (v *Vulnerabilities) filterVulnerabilities(url *url.URL) []Vulnerability {
	filter := VulnerabilityFilter{}
	filter.setVulnerabilityFilter(url)
	filteredVulnerabilities := make([]Vulnerability, 0, len(v.Content))

	// Do some quick-exit sanity checks
	if filter.Limit == 0 {
		return filteredVulnerabilities
	}
	if filter.SeverityAtLeast == 0 && filter.Limit == -1 && filter.Since == "" {
		return v.Content
	}

	limitCount := 0
	for _, vun := range v.Content {
		if vun.Severity < filter.SeverityAtLeast {
			continue
		}

		if filter.Since != "" {
			const layout = "2006-01-02"
			var err error = nil
			reportedTime, err := time.Parse(layout, vun.DateReported)
			if err != nil {
				log.Println(err)
				continue
			}
			sinceTime, err := time.Parse(layout, filter.Since)
			if err != nil {
				log.Println(err)
				continue
			}
			if reportedTime.Before(sinceTime) {
				continue
			}
		}

		limitCount++
		filteredVulnerabilities = append(filteredVulnerabilities, vun)
		if limitCount >= filter.Limit && filter.Limit != -1 {
			return filteredVulnerabilities
		}
	}

	return filteredVulnerabilities
}

// setVulnerabilityFilter sets the state of a VulnerabilityFilter to match the parameters in a URL
func (v *VulnerabilityFilter) setVulnerabilityFilter(url *url.URL) {
	var base int = 0
	var bitSize int = 32
	var err error = nil

	// Convert severity from string in url query to int
	var severity int64 = 0
	if url.Query().Get("severity_at_least") != "" {
		severity, err = strconv.ParseInt(url.Query().Get("severity_at_least"), base, bitSize)
		if err != nil {
			// If the severity string was not a valid number just ignore it
			log.Println(err)
			severity = 0
		}
	}

	// Convert limit from string in url query to int
	var limit int64 = -1
	if url.Query().Get("limit") != "" {
		limit, err = strconv.ParseInt(url.Query().Get("limit"), base, bitSize)
		if err != nil {
			// If the limit string was not a valid number just ignore it
			log.Println(err)
			limit = -1
		}
	}

	v.SeverityAtLeast = int(severity)
	v.Limit = int(limit)

	if url.Query().Get("since") != "" {
		_, err = time.Parse("2006-01-02", url.Query().Get("since"))
		if err != nil {
			// If the since time string was not valid just ignore it
			log.Println(err)
		} else {
			v.Since = url.Query().Get("since")
		}
	}
}
