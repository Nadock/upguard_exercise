# UpGuard Coding Exercise

This is my implementation of the requirements outlined in `golang_coding_exercise.txt`

**Author:** Riley Chase

**Date:** 2017-07-07

## Building the code

Use `go build main.go` to build the executable

## Running the code

By default the program will look for a file named `vulns.json` to load vulnerability definitions from and will respond to requests at `localhost:8080`. To run the code with the default options use:
```
./main
```

To change where the vulnerabilities are loaded from or the port on which the sever will respond to requests use the following command line flags.

| Option to set | Flag | Example Option |
|---------------|------|----------------|
| file | `-file` | `./path/to/file` |
| port | `-port` | `65500` |

Additionally, access usage information on the command line with the `-help` flag.

### Example
To load from the file `new_data.json` and respond on port `9900` use:
```
./main -file=new_data.json -port=9900
```

## Assumptions from initial specification
 * Only vulnerabilities with `date_reported` before `since` are filtered out, vulnerabilities with `date_reported` equal to `since` are included in the response.
 * User input of "erroneous/strange" input results in that parameter being ignored.
   * Eg: `limit=NaN` will mean there is no limit applied to the response
 * "erroneous/strange" input from files or incorrectly set command line parameters cause the program to display an error message and exit
 
