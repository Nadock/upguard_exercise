package main

import "testing"

func TestLoadVulnerabilities(t *testing.T) {
	var testVulns = Vulnerabilities{}
	testVulns.loadVulnerabilities("test_data/valid_test_0.json")
	if len(testVulns.Content) != 2 {
		t.Fail()
	}
	// Test each Vulnerability struct to make sure the JSON data was loaded into them correctly
	// These values are from the test_data/valid_test_0.json file
	for i, v := range testVulns.Content {
		switch i {
		case 0:
			if v.Id != 0 {
				t.Fail()
			}
			if v.Severity != 1 {
				t.Fail()
			}
			if v.Title != "test0" {
				t.Fail()
			}
			if v.DateReported != "1970-00-01" {
				t.Fail()
			}
		case 1:
			if v.Id != 1 {
				t.Fail()
			}
			if v.Severity != 2569887132 {
				t.Fail()
			}
			if v.Title != "test1" {
				t.Fail()
			}
			if v.DateReported != "1994-09-03" {
				t.Fail()
			}
		}
	}
}
