package main

import (
	"net/url"
	"testing"
	"time"
)

func TestFilterVulnerabilities(t *testing.T) {
	var testVuns = Vulnerabilities{}
	testVuns.loadVulnerabilities("test_data/valid_test_1.json")

	/**
	 * Testing of the filterVulnerability function uses JSON data found in test_data/valid_test_1.json
	 *
	 * In general, each test is designed to return a specific number of the vulnerabilities defined in the valid_test_1.json
	 * file. Some tests also check that the returned vulnerabilities match the filter (for limit this is omitted).
	 */

	/**
	 * Test severity_at_least
	 */
	testURL, err := url.Parse("http://localhost:8080?severity_at_least=3")
	if err != nil {
		t.Fatal(err)
	}
	result := testVuns.filterVulnerabilities(testURL)
	if len(result) != 3 {
		t.FailNow()
	}
	for _, v := range result {
		if v.Severity < 3 {
			t.FailNow()
		}
	}

	testURL, err = url.Parse("http://localhost:8080?severity_at_least=0")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?severity_at_least=-1")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?severity_at_least=6")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 0 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?severity_at_least=NaN")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	/**
	 * Test limit
	 */
	testURL, err = url.Parse("http://localhost:8080?limit=0")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 0 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?limit=-1")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?limit=4")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 4 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?limit=NaN")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	/**
	 * Test since
	 */
	testURL, err = url.Parse("http://localhost:8080?since=2017-05-01")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 3 {
		t.FailNow()
	}
	for _, v := range result {
		const layout = "2006-01-02"
		reportedTime, err := time.Parse(layout, v.DateReported)
		if err != nil {
			t.Fatal(err)
		}
		sinceTime, err := time.Parse(layout, "2017-05-01")
		if err != nil {
			t.Fatal(err)
		}
		if reportedTime.Before(sinceTime) {
			t.FailNow()
		}
	}

	testURL, err = url.Parse("http://localhost:8080?since=1970-01-01")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?since=ThisStringIsNotADate")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?since=2020-01-01")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 1 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?since=2117-08-04")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 1 {
		t.FailNow()
	}

	/**
	 * Test miscellaneous bad HTTP parameters
	 */
	testURL, err = url.Parse("http://localhost:8080?password=Hunter2")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?password=")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?password")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 5 {
		t.FailNow()
	}

	/**
	 * Test combined parameters
	 */
	testURL, err = url.Parse("http://localhost:8080?severity_at_least=4&since=2020-01-01")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 1 {
		t.FailNow()
	}
	if result[0].Id != 5 {
		t.FailNow()
	}

	testURL, err = url.Parse("http://localhost:8080?severity_at_least=3&limit=2")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 2 {
		t.FailNow()
	}
	for _, v := range result {
		if v.Severity < 3 {
			t.FailNow()
		}
	}

	testURL, err = url.Parse("http://localhost:8080?limit=2&since=2017-04-05")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 2 {
		t.FailNow()
	}
	for _, v := range result {
		const layout = "2006-01-02"
		reportedTime, err := time.Parse(layout, v.DateReported)
		if err != nil {
			t.Fatal(err)
		}
		sinceTime, err := time.Parse(layout, "2017-04-05")
		if err != nil {
			t.Fatal(err)
		}
		if reportedTime.Before(sinceTime) {
			t.FailNow()
		}
	}

	testURL, err = url.Parse("http://localhost:8080?limit=3&since=2017-04-05&severity_at_least=5")
	if err != nil {
		t.Fatal(err)
	}
	result = testVuns.filterVulnerabilities(testURL)
	if len(result) != 1 {
		t.FailNow()
	}
	if result[0].Id != 5 {
		t.FailNow()
	}
}
